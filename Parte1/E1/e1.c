#include <stdio.h>
#include <stdlib.h>


// Función para realizar la operación de los números al cubo por separado.
void cubo(int num1, int num2){
	// Variables de cubo 1 y 2 respectivamente a num1, num2. 
	int cubo1; 
	int cubo2;
	
	// Operación cálculo.
	cubo1 = num1 * num1 * num1; 
	cubo2 = num2 * num2 * num2;
	
	printf ("El cubo del n° %d es: %d\n", num1, cubo1);
	printf ("El cubo del n° %d es: %d\n", num2, cubo2);
}


// Función para realizar la operación suma de ambos números.
void suma(int num1, int num2){
	// Variable para la suma.
	int suma;
	
	// Operación cálculo.
	suma = num1+num2;
	
	printf ("La suma de los numeros %d y %d es: %d\n", num1, num2, suma); 
}


// Función para realizar la operación de los números al cuadrado por separado.
void cuadrado(int num1, int num2){
	// Variables de cuadrado 1 y 2 respectivamente a num1, num2.
	int c1; 
	int c2;
	
	// Operación cálculo.
	c1 = num1 * num1; 
	c2 = num2 * num2;
	
	printf ("El cuadrado del n° %d es: %d\n", num1, c1);
	printf ("El cuadrado del n° %d es: %d\n", num2, c2);
}


int main(){
	
	int presiona;
	// Mamoneo mío porque te amo uwu.
	printf ("Hola amorcito, al presionar 1 se te pedirá que ingreses dos valores para luego recibir un menú con divertidas opciones, disfrutalo :*\n");
	printf ("\n");
	printf ("                               *  *       *  *       \n");
	printf ("                             *      *   *      *     \n");
	printf ("                             *       * *       *     \n");
	printf ("                              *       *       *      \n");
	printf ("                                *           *        \n");
	printf ("                                  *       *          \n");
	printf ("                                    *   *            \n");
	printf ("                                     * *             \n");
	printf ("                                      *              \n");
	printf ("\n");
	printf ("\n");
	printf ("TE AMO MUCHO");
	printf ("\nPresiona aquí: ");
	scanf ("%d", &presiona); 
	
	if (presiona == 1){
	
		// Variables de números a ingresar y opción a elegir del menú.
		int num1;
		int num2;
		int opcion; 
		
		// Ciclo de menú. 
		do{
			printf ("\nIngrese un número: "); 
			scanf ("%d", &num1); 
			
			printf ("Ingrese siguiente numero: "); 
			scanf ("%d", &num2); 
			
			system("clear");
			
			printf (" Ingrese lo que desee calcular \n");
			printf ("\n");
			printf (" [1] Cuadrado\n"); 
			printf (" [2] Suma\n");
			printf (" [3] Cubo\n"); 
			printf (" [4] elegir nuevos números\n");
			printf (" [5] Salir\n");
			printf ("\n"); 
			printf ("Calcular?:  ");
			scanf ("%d", &opcion);
	
			// Switch para establecer los casos de las opciones del menú.
			switch (opcion){
				// Casos.
				case 1: if (opcion == 1){
							system("clear");
							cuadrado(num1, num2);
						}
						break;
						
				case 2:	if (opcion == 2){
							system("clear");
							suma(num1, num2);
						}
						break;
			
				case 3: if (opcion == 3){
							system("clear");
							cubo(num1, num2);
						}
						break;
						
				case 4: if (opcion == 5){
							exit(-1);
						}		
			}
	
		// Finaliza el ciclo que realiza la funcionalidad de poder elegir nuevamente dos números.
		} while (opcion != 5);
	}
	else{ 
		// Fin del mamoneo >.< muak. 
		printf ("Te dije que solo presionaras 1 wey x.x");	 
	}
	
	return 0;	
}
