>> Ejercicio 1

Al ejecutar se mostrará una indicación simple la cual solo se deberá acceder al siguiente menú si presionas el número 1, Luego aparecerá la opción de ingresar dos números distintos o iguales y se abrirá un nuevo menú con las opciones de calcular el cuadrado, cubo y la suma entre ambos números como tambien salirse de programa. al momento de elegir una opcion inmediatamente se volveran a pedir dos números y así hasta que el usuario desee salir presionando UNICAMENTE la tecla de n° 5. 

* Cabe destacar que las teclas restantes del teclado no serán aptas para el programa, es decir, si se pide el ingreso de números no se debe presionar letras y si se pide salir con el número 5 no se debe presionar ninguna otra, por otro lado el programa no funcionará. 

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --

Para compilar el código se debe agregar el comando: gcc e1.c -lm -o e1
Para ejecutar el código se debe agregar el comando: ./e1

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Grax amorcito por ayudarme a aprender <3. 
