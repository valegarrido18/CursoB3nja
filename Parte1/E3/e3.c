#include <stdio.h>
#include <stdlib.h>


// Función que realiza el calculo del promedio y el ingreso de notas. 
void calpromedio(float promedio, float nota, char *alumno){
	// Variables inicializadas en 0 para realizar el cálculo de la suma y división.
	float aux=0;
	float contador=0;
	
	// Ciclo para el ingreso de notas que se detiene unicamente con la tecla n°0.
	do {
		printf ("\nIngrese Nota: ");
		scanf ("%f", &nota);
		// Se suman las notas ingresadas.
		aux = aux + nota;
		// Se cuenta la cantidad de notas que estan siendo ingresadas.
		contador = contador + 1;
		// Cálculo del promedio.
		promedio = aux/(contador-1);
	} while (nota != 0); 
		
	system("clear");
	printf("El promedio de %s es: %.1f", alumno, promedio);

}


int main(){
	// Variable tipo char para ingresar nombre del alumno con límite hasta 100 carácteres.
	char alumno[100];
	// Variables inicializadas en 0.
	float promedio=0;
	float nota=0; 
	
	printf ("Ingrese nombre del alumno: ");
	scanf ("%s", alumno);
	printf ("\n");
	printf("\n *Se iniciará el ingreso de notas, para dejar de ingresar notas y calcular el promedio del alumno presione 0.\n");
	
	// Se llama a función que realiza el calculo del promedio y el ingreso de notas. 
	calpromedio(promedio, nota, alumno);
					
	return 0;
}
