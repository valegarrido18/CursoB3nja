>> Ejercicio 1

Al ejecutar se pedirá el nombre del alumno que desee, luego se explicará la instrucción para detener el ingreso de notas y el calculo del promedio del alumno el cual deberá ser únicamente con la tecla n°0. luego se imprimirá el promedio. 

*IMPORTANTE 
Este ejercicio fue reducido a que se ingresara un solo alumno debido a los pocos conocimientos del programador :D. Recordar además que las notas no puden ingresarse a modo de decimal.

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --

Para compilar el código se debe agregar el comando: gcc e3.c -lm -o e3
Para ejecutar el código se debe agregar el comando: ./e3

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Grax amorcito por ayudarme a aprender <3. 
