>> Ejercicio 2

Al ejecturarse se explicará las condiciones para poder calcular la hipotenusa de un triángulo rectángulo, luego de esto se indica que ingrese los catetos de dicho triangulo especificando cual será el cateto de la base y altura para poder hacer más sencillo el cálculo del área y perimetro haciendo que se cumpla de esta forma el desafío del ejercicio. 

*Para ingresar números decimales como por ejemplo 1,5 se debe cambiar la coma que se utiliza normalmente por un punto, si no, el código no funcionará de manera correcta.

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --

Para compilar el código se debe agregar el comando: gcc e2.c -lm -o e2
Para ejecutar el código se debe agregar el comando: ./e2

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Grax amorcito por ayudarme a aprender <3. 
