#include <stdio.h>
#include <stdlib.h>
#include <math.h>


// Función para realizar el cálculo del área.
void calculoa(float c1, float c2){
	// Variable de tipo float. 
	float area;
	// Operación cálculo.
	area = (c2*c1)/2;
	
	printf("-> El área del T. rectángulo es: %.2f\n", area);
}


// Función para realizar el cálculo del perímetro.
void calculop(float c1, float c2, float raiz){
	// Variable de tipo float para realizar operaciones con decimales.
	float perimetro;
	
	// Operación cálculo.
	perimetro = c1 + c2 + raiz;
	
	printf("-> El perímetro del T. rectángulo es: %.2f\n", perimetro);
	printf("\n");
	
	// Llamado a función que realiza siguiente cálculo.
	calculoa(c1,c2);
	
}


// Función para realizar el cálculo de la hipotenusa.
void calculohipo(float c1, float c2){
	// Variables de tipo float para realizar operaciones con decimales.
	float hipotenusa;
	float raiz;
	
	// Operación cálculo.
	hipotenusa = c1*c1 + c2*c2;
	raiz = sqrt(hipotenusa); 
	
	printf("-> La hipotenusa del T. rectángulo es: %.2f\n", raiz);
	printf("\n");
	
	// Llamado a función que realiza siguiente cálculo.
	calculop(c1,c2,raiz);
	
}


int main(){
	// Variables de tipo float (ingreso de decimales) para catetos 1 y 2 para así definir altura y base.
	float c1;
	float c2;
	
	printf("*Para calcular la hipotenusa de un triangulo rectángulo se debe ingresar el valor de ambos catetos\n");
	printf("\n");
	printf("Primer cateto (altura): "); 
	scanf("%f", &c1);
	
	printf("Segundo cateto (base): "); 
	scanf("%f", &c2);
	
	system("clear");
	
	// Llamado de función que realiza cálculos.
	calculohipo(c1,c2);

	
	return 0;
}
